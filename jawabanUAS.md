# no. 1
Berikut adalah Link untuk melihat use case : [Link_Usecase_ClassDiagram](Dokumentasi_FLIP/Use_Case_dan_Class_Diagram.pdf)


# no. 2
Berikut adalah Link untuk melihat class diagram : [Link_Usecase_ClassDiagram](Dokumentasi_FLIP/Use_Case_dan_Class_Diagram.pdf)


# no. 3  Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
Jawab: 

Berikut adalah implementasi SOLID Design Pettern: 
![Dokumentasi_FLIP](Dokumentasi_FLIP/implementasi_solid.png)


Implementasi SOLID Design Principle pada program saya adalah:  
1.	Single Responsibility Principle (SRP) yaitu setiap kelas memiliki tanggung jawab yang jelas(tanggung jawab terbatas), implementasinya terletak pada Kelas DatabaseConnector bertanggung jawab untuk mengelola koneksi ke database, mengeksekusi query, dan mengambil hasil query. Kelas DataAPI yang bertanggung jawab untuk mengambil data dari database menggunakan DatabaseConnector. Dan kelas MyApp yang bertanggung jawab untuk mengatur aplikasi FastAPI, menginisialisasi DatabaseConnector dan DataAPI, serta menjalankan aplikasi menggunakan uvicorn.
2.	Open Closed Principle (OCP): Kode dapat diperluas fungsionalitasnya tanpa mengubah kode yang sudah ada. Implementasinya yaitu pada Kelas DatabaseConnector dan DataAPI dapat digunakan dengan fleksibel dengan menambahkan metode baru jika diperlukan.
3.	Liskov Substitution Principle (LSP): objek-objek dalam program harus dapat diganti dengan instansi kelas turunan mereka tanpa mengubah kebenaran dari program tersebut. pada program diatas tidak ada implementasi LSP 
4.	Interface Segregation Principle (ISP): memisahkan interface yang besar menjadi beberapa interface yang lebih kecil untuk kebutuhan klien tertentu. Pada kode diatas tidak ada implementasi ISP
5.	Dependency Inversion Principle (DIP): Kode harus bergantung pada abstraksi, bukan pada implementasi. Implementasinya terletak pada kelas DataAPI menerima objek db_connector dari kelas DatabaseConnector melalui konstruktor. Selin itu terletak juga pada kelas MyApp menerima objek app dari FastAPI dan objek db_config dari konfigurasi database melalui konstruktor.


# no. 4	Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

Jawab: 

![Dokumentasi_FLIP](Dokumentasi_FLIP/implementasi_solid.png)
![Dokumentasi_FLIP](Dokumentasi_FLIP/implementasi_pattern.png)
Penggunaan Design Pettern MVC yaitu  pola desain yang memisahkan logika aplikasi menjadi tiga komponen utama: Model, View, dan Controller, berikut adalah uraiannya:

1.	Model: terletak pada kelas DatabaseConnector yang bertanggung jawab untuk mengelola koneksi ke database dan mengeksekusi query(merupakan "model" yang menangani operasi yang berkaitan dengan data). Selain itu juga terdapadat pada kelas Transferframe yangbertindak sebagai Model dengan mengatur koneksi ke database dan mengimplementasikan operasi CRUD (Create, Read, Update, Delete) pada tabel "tbtransfer".
2.	View: MyApp menggunakan FastAPI untuk mengekspose API endpoint ("/") yang dapat dipanggil dari sisi klien untuk mengakses data dari database.terdapat juga pada tampilan grafis yang dihasilkan Java Swing. Tampilan ini untuk mengisi formulir dengan detail transfer dan menampilkan data transfer yang ada dalam sebuah tabel.
3.	Controller: terletak pada Kelas DataAPI bertindak sebagai "controller" yang berfungsi sebagai jembatan antara model (DatabaseConnector) dan view (FastAPI). metode yang mengatur logika dan interaksi pengguna, seperti pengisian formulir, penambahan data transfer, pengeditan, penghapusan, dan navigasi ke halaman berikutnya


# no. 5 Mampu menunjukkan dan menjelaskan konektivitas ke database
Jawab: 

Saya menggunakan mySQL untuk menejement datanya, Langkah awalnya adalah mengaktifkan Apache dan MySQL pada Xammp lalu membuat database bernama ‘flipdatabase’ dan membuat table bernama ‘tbtransfer’ dan ‘tb_user’ dimana masing masing memiliki instance ![Dokumentasi_FLIP](Dokumentasi_FLIP/mysql_dokumentasi.png)

Dilanjutkan membuat project pada netbeans kemudian membuat package Koneksi dan class Koneksi.java , lalu import library MySQL JDBC Driver ![Dokumentasi_FLIP](Dokumentasi_FLIP/jdbc_dokumentasi.png)


![Dokumentasi_FLIP](Dokumentasi_FLIP/koneksi_dokumentasi.png)

![Dokumentasi_FLIP](Dokumentasi_FLIP/kodekoneksi_dokumentasi.png)


Setelah itu lakukan run pada kode diatas dan pastikan database sudah berhasil dikoneksikan
![Dokumentasi_Flip](Dokumentasi_FLIP/koneksisukses_dokumentasi.png)



# no. 6 Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya 
Jawab:



- Pada webservice yang dibuat yaitu mengambil data dari database yang terhubung dengan GUI dimana di GUI sudah dilengkapi dengan operasi CRUD; simpan,hapus,dan edit yang masing masing tombol mampu menjalankan tugasnya.![Dokumentasi_Flip](Dokumentasi_FLIP/transfer_dokumentasi.png)

- Untuk webservice saya menggunakan Fastapi, dimana isinya mengambil data dari database yang terkoneksi dengan GUI. Berikut adalah kodenya ![Dokumentasi_FLIP](Dokumentasi_FLIP/implementasi_solid.png)


- Menjalankannya menggunakan perintah uvicorn apiku:MyApp –reload pada direktori file Apiku.py tersimpan lalu jalankan  http://127.0.0.1:8000 untuk melihat file json berikut tampilannya 
![Dokumentasi_FLIP](Dokumentasi_FLIP/terminal_dokumentasi.png)
![Dokumentasi_FLIP](Dokumentasi_FLIP/json_dokumentasi.png)


# no. 7 Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital


![Dokumentasi_FLIP](Dokumentasi_FLIP/VIDEO_UAS_PRAK_PBO.mp4)


Saat melakukan Sign Up, pengguna diminta untuk menginputkan username, nomor telepon, dan password. Setelah pengguna melengkapi informasi tersebut dan mengklik Sign Up, aplikasi akan menampilkan tampilan Login. Di sini, pengguna diminta untuk mengisi nomor telepon dan password. Jika nomor telepon dan password sesuai, aplikasi akan menampilkan menu yang berisi berbagai transaksi, seperti transfer, e-wallet, tagihan listrik, dan pembelian pulsa. Pengguna bebas memilih transaksi mana yang ingin dilakukan.

Ketika pengguna mengklik **menu transfer**, aplikasi akan menampilkan informasi bank tujuan, nomor rekening, jumlah transfer, dan metode transfer. Pengguna diminta untuk mengisi data sesuai dengan keinginannya. Setelah mengklik Simpan dan Lanjutkan, aplikasi akan menampilkan output berupa nomor rekening Flip dan total transfer. Total transfer ini mencakup jumlah transfer beserta kode unik. Setelah pengguna selesai melakukan transfer, mereka dapat mengklik tombol 'Saya sudah transfer'. Aplikasi akan menampilkan pesan yang memberitahu bahwa transfer telah diteruskan ke penerima.

Aplikasi ini menyediakan berbagai menu transaksi seperti **Transfer, E-wallet, Tagihan Listrik, dan Pembelian Pulsa** . Setiap menu memiliki langkah-langkah yang sama dalam pengisian data, yaitu pengguna diminta untuk menginputkan informasi yang relevan, seperti nomor tujuan, jumlah transfer, dan metode pembayaran. Setelah pengguna mengklik Simpan dan Lanjutkan, aplikasi akan menghasilkan nomor rekening Flip dan total transfer yang mencakup kode unik. Setelah pengguna melakukan transfer, mereka dapat mengkonfirmasi dengan mengklik tombol "Saya sudah transfer", dan aplikasi akan memberikan pesan bahwa transfer telah diteruskan ke penerima


# no. 8 Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital 
Jawab: 


HTTP connection terjadi saat FastAPI dijalankan dan menerima permintaan HTTP dari klien. Pada saat itu, FastAPI akan mengeksekusi fungsi get_data_from_database yang dikaitkan dengan metode GET dan rute /. Ini akan menjalankan operasi get_data_from_database dalam objek DataAPI. Ketika permintaan GET diterima, get_data_from_database dalam DataAPI akan mengambil data dari tabel "tbtransfer" dengan menjalankan query SQL SELECT * FROM tbtransfer menggunakan metode execute_query dalam objek DatabaseConnector. Hasilnya kemudian dikembalikan sebagai respons HTTP yang dikirimkan kembali ke klien.



# no. 9

https://youtu.be/ZdlFQ2nwFRA



